"""
Create dataset annotation file

run as python3 -m scripts.create_annotation data/path

data/path is optional

JCA
"""
import os
import sys
import json

from tqdm import tqdm


# Run from main
DATAPATH = 'data'

def is_image(filename, verbose=False):
    """ Check if image file is valid"""

    data = open(filename,'rb').read(10)

    # check if file is JPG or JPEG
    if data[:3] == b'\xff\xd8\xff':
        if verbose == True:
             print(filename+" is: JPG/JPEG.")
        return True

    # check if file is PNG
    if data[:8] == b'\x89\x50\x4e\x47\x0d\x0a\x1a\x0a':
        if verbose == True:
             print(filename+" is: PNG.")
        return True

    # check if file is GIF
    if data[:6] in [b'\x47\x49\x46\x38\x37\x61', b'\x47\x49\x46\x38\x39\x61']:
        if verbose == True:
             print(filename+" is: GIF.")
        return True

    return False

def get_encode(folder_name, categories):
    """Generate encode for images in folder"""
    cats = folder_name.split('|')
    return [1 if  c in cats else 0 for c in categories]


def create(datapath, debug=False):
    # datapath = os.path.abspath(datapath)
    print(f'Dataset path: {datapath}')
    folders = os.listdir(datapath)

    # Get different classes
    # each folder class is split by |
    categories = list(set([c for f in folders for c in f.split('|')]))
    print(f'Categories on dataset: {categories}')

    print('Generating annotation file...')
    dataset = [] # (im_path, encode)
    for f in tqdm(folders):
        f_path = os.path.join(datapath, f)
        if os.path.isdir(f_path):
            path_encode = get_encode(f, categories)
            for im in os.listdir(f_path):
                im_path = os.path.join(f_path, im)
                if is_image(im_path):
                    dataset.append((im_path, path_encode))
                else:
                    if debug:
                        print(f'Skipping {im_path}. Not valid image')
                    else:
                        pass
    savepath = os.path.join('annotation.json')
    print(f'Saving annotation path on: {savepath}')
    with open(savepath, 'w') as f:
        json.dump({
           'dataset': dataset,
           'categories' : categories 
        },f)
    print('Done!')

                

    


if __name__ == '__main__':
    print('Creating annotation file...')
    datapath = DATAPATH if len(sys.argv) < 2 else sys.argv[1]
    create(datapath)
