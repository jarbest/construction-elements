"""
Calculate dataset statistics using the annotation file

JCA
"""
import sys
import json
import os

from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

def generate_stats(annotation_file):
    print(f'Generating datasets statistics from: {annotation_file}')    

    with open(annotation_file, 'r') as f:
        ann = json.load(f)
    dataset = ann['dataset']
    cats = ann['categories']
    print(f'- Dataset size: {len(dataset)}.\n- Dataset Categories: {cats}')

    count = np.array([0 for i in cats])
    print('Counting classes...')
    for d in tqdm(dataset):
        count += np.array(d[1])
    
    print('Plotting...')
    fig = plt.figure(figsize = (10, 5))
    plt.bar(cats, count, width = 0.4)
    
    plt.xlabel("Dataset categories")
    plt.ylabel("No. Observations ")
    plt.title("Number of observations for categories")

    plt.savefig(os.path.join('dataset_plots', 'num_categories.png'))

    plt.show()



if __name__ == '__main__':
    annotation_file = sys.argv[1]
    generate_stats(annotation_file)
